#!/usr/bin/python3
import asyncio
import aiocoap
import argparse

import time

N = 0

async def read_sensor(protocol, sensor_addr, port = 5683):
    global N
    old = time.time()
    request = aiocoap.Message(code=aiocoap.GET, uri='coap://[%s]:%d/riot/board' % (sensor_addr, port))
    ok=1
    try:
        response = await protocol.request(request).response
    except Exception as e:
        ok=0

    cur_time = time.time();
    elapsed = cur_time - old
    print("%f,%d,%s,%d,%f" % (cur_time, N, sensor_addr, ok, elapsed))
    N += 1

async def main(args):
    protocol = await aiocoap.Context.create_client_context()
    aiocoap.numbers.constants.REQUEST_TIMEOUT = 30

    while (True):
        await read_sensor(protocol, args.address)
        await asyncio.sleep(args.sleep)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Read sensor.')
    parser.add_argument('--address', '-a', type=str,
                    help='sensor IPv6 address')
    parser.add_argument('--sleep', '-s', type=float,
                    help='sleep time')
    args = parser.parse_args()

    asyncio.get_event_loop().run_until_complete(main(args))