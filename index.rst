🚀 (Jupyter) Tutorials
=======================

.. toctree::
    :maxdepth: 1
    :caption: Contents:

    setup_for_use_in_labs.iot-lab.info.ipynb
    setup_for_use_in_g5k.ipynb
    g5k/index.rst
    fit_and_g5k/index.rst
